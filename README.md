Food Chain: Eduard Nurmukhametov a Khomutov Mikhail

Funkční požadavky:

    F1. Hlavní entity se kterými pracujeme je Zemědělec nebo Farmář, Zpracovatel, Sklad, Prodejce, Distribuce, Zákazník, Potravina. 

    F2.	Jednotlivé strany (parties) si v ekosystému předávají potraviny a za potraviny/operace s nimy si platí penězy. Každá strana s potravinou provede některé operace. Každá operace má parametry - např. operace Skladování - parametry: délka 4 dny, teplota 12 stupňů, vlhkost ….. Operace provedená s určitými parametry a určitou Party = Transakce. 

    F3.	Systém je realizován pomocí velmi zjednodušené block chain platformy. Platforma je zjednodušená pro realizaci funkčních požadavků z tohoto seznamu. Každá Transakce s parametry a identifikací party, která je provedla, je zaznamenána a odkazuje se na předchozí Transakci. Již provedené Transakce a jejich parametry nelze zpětně upravovat. 
    
    F4.	V každém bodě životního cyklu potraviny lze získat věrohodné informace o tom, přes jaké parties potravina prošla a jaké operaci s ní provedly. Systém umožňuje tyto informace vygenerovat ve formě reportu (textový soubor) 

    F5.	Systém realizuje tzv. Kanály. Kanál spojuje parties, operace a má také svůj vlastní řetěz událostí. Příkladem je chanel na výměnu zeleniny, channel na maso, a channel pro platby. Každá operace má nastaveno v jakém channel se může provést, stejně tak Party má definováno v jakých kanálech je účastníkem (participant). 

    F6.	Systém detekuje tzv. double spending problém. Tuto detekci odsimulujete na vámi zvoleném scénáři, např. kdy se zpracovatel snaží prodat tu samou potravinu 2x pod stejným certifikátem (dokládající původ potraviny). 

    F7.	Systém detekuje pokus o zpětnou změnu do řetězce událostí. Tuto detekci odsimulujete na vámi zvoleném scénáři. 

    F8.	Zpracování potraviny u party může být realizováno stavovým automatem - tedy potravina prochází různými stavy než může být předána další party. Mezi jednotlivými diskrétními kroky simulace (vysvětleno níže) může dojít pouze k jednomu přechodu mezi stavy. 

    F9.	Do kanálů lze rozesílat požadavky, které obdrží Parties, které jsou účastníky v daných kanálech. Např. požadavek Chtěl bych 150kg masa. Parties na požadavky mohou a nemusí reagovat. Parties se mohou registrovat/odregistrovat buď do/z celého kanálu nebo na typ/z typu požadavku v rámci kanálu. 

    F10. Ze systému je možné vygenerovat následující reporty: 

        ○	Parties report: Jaké parties se podílely na procesování daných potravin, jak dlouho se u nich potraviny zdržely, jakou marži si party aplikovala na vstupní cenu 
        ○	Food chain report (viz funkční požadavek výše): pro potravinu se vypíše přes jaké parties prošla a jaké operace s jakými parametry s ní provedla 
        ○	Security report: Jaké parties se snažily podvrhnout původ potravin nebo provést tzv. Double spending a kolikrát. 
        ○	Transaction report - pro každý diskrétní krok ekosystému vypíše transakce které byly provedeny, kým a kolik měla každá party v ekosystému peněz a potravin

Funkcní požadavky:

    F1: src/main/java/foodchain/party

    F2: (foodchain/party package) Processor sets parameters also all parties when the do their
    transaction(For example addSellTransaction(ProductChannel)) they change state of product

    F3: (foodchain/transactions package) Transaction has previous transaction and also has hashes
    made with previous transaction(what also has it's own hash)

    F4: Food Chain report, in any part of code you can sk to show it and it will do it

    F5: (foodchain/channels package)ProductChannel, MoneyChannel what are extending abstract 
    class Channel

    F6: (foodchain/party package)When someone(Seller) does Sell transaction(ProductChannel) we
    are checking Uuid of product with Uuid of previous products

    F7: Nevim, nerozumim asi

    F8: When someone does transaction in transaction the state of product changes:

    F9: (channels/util package package) Requests, method addRequest(ProductChannel) makes it 
    possible
    
    F10: (foodchain package) Reporter: addPartiesReport - showPartiesReport, 
    addFoodChainReport - showFoodChainReport, addSecurityReport - showSecurityReport, 
    addTransactionReport - showTransactionReport

Nefunkční požadavky:

    ●   Není autentizace ani autorizace

    ●	Aplikace nemá GUI. Aplikace komunikuje s uživatelem pomocí výpisů do souboru

    ●	V systému neexistuje centrální autorita, přes kterou by se prováděly transakce

    ● Aplikaci běží v jednom threadu, kdy se postupně aplikují všechny transakce provedené 
    jednotlivými parties v rámci aktuálního kroku diskrétní simulace.

    ●	Myslim ze všechno je dobře ukryté

    ●	Namísto PKI infrastruktury, public a private klíčů, šifrování/dešifrování pracujeme s 
    Uuid. Každy product ma svoje Immutable Uuid(foodchain/Products package, Object Product)

    ●	Reporty jsou generovány do textového souboru
    
    ●	Program běží v Mainu package builder



Design patterns:
    
    Singleton: Reporter
    
    Builder: package builder: FoodChainBuilder

    Iterator: package transactions: TransactionIterator

    State: package products: ProductState

    Observer: package channels: PartyObservable, package party: ChannelObserver

    Factory Method: package praty/farmer ProductFactory

    Strategy: package products: ParametersStrategy
    

Discretni krok 0:      

    Customer makes request for product(2kg of "BEEF") addressed to Seller

    Seller answers and does not find product in his products -> he makes request adressed to 
    Storage

    Storage answers and does not find product -> it makes request addressed to Processor

    Processor answers and does not find product -> he makes request addressed to Farmer

    Farmer answers and creates a product and asks distributor to deliver it to Processor

    Distributor delivers product to Processor

    Processor sets Parameters strategy and asks distributor to deliver product to Storage

    Distributor delivers product to Storage

    Storage asks distributor to deliver product to Seller

    Distributor delivers product to Seller


    Here important moment: we are doing the same product with same Uuid, in all - copy

    it saves inside of other seller, let's call him Albert(Seller)


    Seller asks distributor to deliver product to Customer

    Distributor delivers product to Customer

Discretni krok 1:

    Customer makes again request for product(2kg of "BEEF") addressed to Seller

    Albert(Seller) answers and when he makes Sell Transaction, double-spending error appears

