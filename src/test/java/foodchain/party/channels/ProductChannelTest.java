package foodchain.party.channels;

import foodchain.party.channels.util.Request;
import foodchain.party.*;
import foodchain.product.Product;
import foodchain.product.ProductState.SoldState;
import foodchain.product.Products.MeatProduct;
import foodchain.product.Products.MeatProducts;
import foodchain.product.Products.PlantProduct;
import foodchain.product.Products.PlantProducts;
import foodchain.transactions.TransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class ProductChannelTest {

    @Test
    void addRequest_requestToChannel_requestInChannelAndSellerSellerAndCustomerAreSubscribed() {
        //Arrange
        ProductChannel channel = new ProductChannel(TransactionType.PRODUCT);
        Party seller = new Seller("Captain Amerika", 250, 30);
        Party customer = new Customer("Iron Man", 300);
        Request request = new Request(customer, new MeatProduct(5, MeatProducts.BEEF), PartyType.SELLER);

        //Act
        channel.attach(seller);
        channel.attach(customer);
        channel.addRequest(request);

        //Assert
        Assertions.assertTrue(channel.requests.contains(request));
        Assertions.assertTrue(seller.requests.contains(request));

        Assertions.assertTrue(channel.subscribers.contains(seller));
        Assertions.assertTrue(channel.subscribers.contains(customer));
    }

    @Test
    void addDistributionTransaction_MockedPartiesProductStateIsNull_DistributionTransactionAddedAndProductStateChanged() {
        //Arrange
        ProductChannel channel = new ProductChannel(TransactionType.PRODUCT);
        Product product = new Product(new PlantProduct(2, PlantProducts.ONION));
        Party creator = Mockito.mock(Seller.class);
        Party receiver = Mockito.mock(Customer.class);
        Party distributor = Mockito.mock(Distributor.class);
        Mockito.when(distributor.getPartyType()).thenReturn(PartyType.DISTRIBUTOR);

        //Act
        channel.addDistributionTransaction(creator, receiver, distributor, product);

        //Assert
        Assertions.assertSame(channel.lastTransaction.getTransactionType(), TransactionType.DISTRIBUTION);
        Assertions.assertSame(channel.lastTransaction.getCreator(), creator);
        Assertions.assertSame(channel.lastTransaction.getPreviousTransaction().getTransactionType(), TransactionType.GENESIS);
        Assertions.assertTrue(product.getProductState() instanceof SoldState);
    }
}