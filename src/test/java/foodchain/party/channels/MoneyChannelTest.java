package foodchain.party.channels;

import foodchain.party.channels.util.Payment;
import foodchain.party.Customer;
import foodchain.transactions.Transaction;
import foodchain.transactions.TransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MoneyChannelTest {

    @Test
    void addPayment_Bob200AndClient0_Bob0AndClient200NumOfTransactions3() {
        //Arrange
        Customer bob = new Customer("Bob", 200);
        Customer client = new Customer("Client", 0);
        int expectedNumOfTransactions = 3; // With 2 transactions by 100 each and genesis block
        float expectedMoneyCustomer1 = 0;
        float expectedMoneyCustomer2 = 200;

        //Act
        MoneyChannel moneyChannel = new MoneyChannel(TransactionType.MONEY);
        moneyChannel.attach(bob);
        moneyChannel.attach(client);

        moneyChannel.addPaymentTransaction(new Payment(bob, client, 100));
        moneyChannel.addPaymentTransaction(new Payment(bob, client, 100));
        moneyChannel.addPaymentTransaction(new Payment(bob, client, 100));

        Transaction t = bob.getLastTransactionMoney();    //The last block of chain
        int counter = 0;
        while (t != null) {
            System.out.println(t + " :: NumOfTransaction: " + (counter + 1));
            t = t.getPreviousTransaction();
            counter++;
        }

        //Assert
        Assertions.assertEquals(expectedNumOfTransactions, counter);
        Assertions.assertEquals(expectedMoneyCustomer2, client.getBalance());
        Assertions.assertEquals(expectedMoneyCustomer1, bob.getBalance());
    }
}