package foodchain.party;

import foodchain.party.channels.ProductChannel;
import foodchain.party.channels.util.Request;
import foodchain.party.farmer.ProductFactory;
import foodchain.product.Product;
import foodchain.product.Products.MeatProduct;
import foodchain.product.Products.MeatProducts;
import foodchain.product.Products.ProductTypes;
import foodchain.transactions.TransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

class DistributorTest {

    @Test
    void processRequest_SellerHasProduct_CustomerReceiveProduct() {
        //Arrange
        ProductChannel productChannel = new ProductChannel(TransactionType.PRODUCT);
        Party seller = new Seller("Bob", 900, 70);
        Party customer = new Customer("Client", 10000);
        Distributor distributor = new Distributor("Taxi", 0, 20);
        MeatProduct meatProduct = new MeatProduct(2, MeatProducts.BEEF);
        Request request = new Request(customer, new MeatProduct(2, MeatProducts.BEEF), PartyType.SELLER);
        request.setRespondingDistributor(distributor);
        //Mock
        ProductFactory mockedMeatFactory = Mockito.mock(ProductFactory.class);
        Mockito.when(mockedMeatFactory.factoryMethod(ArgumentMatchers.anyFloat(), ArgumentMatchers.any(MeatProducts.class))).
                thenReturn(new Product(new MeatProduct(10, MeatProducts.BEEF)));

        int expectedAmount = 2;
        int expectedRestInSeller = 8;
        float expectedDistributorBalance = request.getProductType().getCost() * (1F + (request.getRespondingDistributor().getMargin() / 100F));
        ProductTypes expectedType = MeatProducts.BEEF;

        //Act
        productChannel.attach(seller);
        productChannel.attach(customer);
        productChannel.attach(distributor);

        productChannel.addRequest(request);
        seller.products.add(mockedMeatFactory.factoryMethod(10, MeatProducts.BEEF));
        seller.fulfillRequest(request);

        Request request1 = distributor.requests.remove();
        distributor.requests.clear();

        distributor.processRequest(request1);

        //Assert
        // Checking Customer for having beef after all Transactions seller and distributor made
        Assertions.assertEquals(expectedAmount, customer.products.get(0).getProductType().getQuantity());
        Assertions.assertEquals(expectedType, customer.products.get(0).getProductType().getProductTypes());
        Assertions.assertEquals(expectedDistributorBalance, distributor.balance);

        // Seller has 10 of Beef. He sold 2 of Beef. 8 of Beef should be in Seller
        Assertions.assertEquals(MeatProducts.BEEF, seller.products.get(0).getProductType().getProductTypes());
        Assertions.assertEquals(expectedRestInSeller, seller.products.get(0).getProductType().getQuantity());

        // Distributor should not have any products
        Assertions.assertTrue(distributor.products.isEmpty());
    }
}