package foodchain.party;

import foodchain.product.Product;
import foodchain.product.Products.MeatProduct;
import foodchain.product.Products.ProductType;
import foodchain.transactions.GenesisTransaction;
import foodchain.transactions.MoneyTransaction;
import foodchain.transactions.SellTransaction;
import foodchain.transactions.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class PartyTest {
    @Test
    void update_sendingMoney_seller300Customer0() {
        //Arrange
        Party seller = new Seller("Bob", 200, -40);
        Party customer = new Customer("Captain", 100);

        ProductType mockedMeat = Mockito.mock(MeatProduct.class);
        Product mockedProduct = Mockito.mock(Product.class);
        Mockito.when(mockedProduct.getProductType()).thenReturn(mockedMeat);

        Transaction transaction1 = new SellTransaction(seller, customer, mockedProduct,
                mockedProduct.getProductType(), new GenesisTransaction(null, null));

        Transaction transaction2 = new MoneyTransaction(customer, seller, 100,
                new GenesisTransaction(null, null));

        int expectedSellerBalance = 200 + 100;
        int expectedCustomerBalance = 100 - 100;

        //Act
        seller.update(transaction1); // if transaction is not money
        seller.update(transaction2); // usual Money transaction
        customer.update(transaction2);

        //Assert
        Assertions.assertEquals(expectedSellerBalance, seller.getBalance());
        Assertions.assertEquals(expectedCustomerBalance, customer.getBalance());
    }
}