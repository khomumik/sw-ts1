package foodchain.party;

import foodchain.party.channels.ProductChannel;
import foodchain.party.channels.util.Request;
import foodchain.party.farmer.Farmer;
import foodchain.product.Product;
import foodchain.product.Products.MeatProduct;
import foodchain.product.Products.MeatProducts;
import foodchain.product.Products.PlantProduct;
import foodchain.product.Products.PlantProducts;
import foodchain.transactions.TransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class SellerTest_procesniTesty {

    @Test
    void processRequest_PROCESS_TEST_combination1() {
        //Arrange
        Party seller = new Seller("Bob", 0, 20);
        Customer customer = new Customer("Svejk", 400);
        Farmer mockedFarmer = Mockito.mock(Farmer.class);
        Mockito.when(mockedFarmer.getPartyType()).thenReturn(PartyType.FARMER);

        ProductChannel productChannel = new ProductChannel(TransactionType.PRODUCT);
        Product beef1 = new Product(new MeatProduct(2, MeatProducts.BEEF));
        Product beef2 = new Product(new MeatProduct(2, MeatProducts.BEEF));
        Product pork = new Product(new MeatProduct(2, MeatProducts.PORK));
        Product onion = new Product(new PlantProduct(2, PlantProducts.ONION));

        Request request = new Request(customer, new MeatProduct(2, MeatProducts.BEEF), PartyType.SELLER);

        int expectedAmountOfProductsSeller = 3;
        int expectedRequestsSizeSeller = 1;
        int expectedRequestsSizeCustomer = 0;
        int expectedDoubleSpending = 0;

        //Act
        seller.receiveProduct(beef1);
        seller.receiveProduct(beef2);
        seller.receiveProduct(pork);
        seller.receiveProduct(onion);

        //Act
        productChannel.attach(seller);
        productChannel.attach(customer);

        productChannel.addRequest(request);

        productChannel.addSellTransaction(seller,customer, seller.products.get(2), new PlantProduct(2, PlantProducts.ONION));
        productChannel.addStoreTransaction(mockedFarmer, seller.products.get(2));
        productChannel.addSellTransaction(seller,customer, seller.products.get(1),new MeatProduct(2, MeatProducts.PORK));

        // Main Act
        seller.fulfillRequest(seller.requests.element());

        //Assert
        Assertions.assertEquals(seller.products.size(), expectedAmountOfProductsSeller);
        Assertions.assertTrue(productChannel.getSubscribers().contains(seller));
        Assertions.assertTrue(productChannel.getSubscribers().contains(customer));
        Assertions.assertEquals(expectedRequestsSizeSeller, seller.requests.size());
        Assertions.assertEquals(expectedRequestsSizeCustomer, customer.requests.size());
        Assertions.assertEquals(expectedDoubleSpending, productChannel.getDoubleSpending().size());

        Assertions.assertTrue(request.isPaid());
        Assertions.assertSame(request.getCreator(), customer);
        Assertions.assertSame(seller.getPartyType(), request.getPartyType());
    }

    @Test
    void processRequest_PROCESS_TEST_combination6() {
        //Arrange
        Party seller = new Seller("Bob", 0, 20);
        Customer customer = new Customer("Svejk", 0);

        ProductChannel productChannel = new ProductChannel(TransactionType.PRODUCT);
        seller.products.add(new Product(new MeatProduct(2, MeatProducts.BEEF)));
        seller.products.add(new Product(new MeatProduct(2, MeatProducts.PORK)));
        seller.products.add(new Product(new PlantProduct(2, PlantProducts.ONION)));

        Request request = new Request(customer, new MeatProduct(2, MeatProducts.BEEF), PartyType.SELLER);

        int expectedAmountOfProductsSeller = 3;
        int expectedRequestsSizeSeller = 1;
        int expectedRequestsSizeCustomer = 0;
        int expectedDoubleSpending = 0;

        //Act
        productChannel.attach(seller);
        productChannel.attach(customer);

        productChannel.addRequest(request);

        seller.fulfillRequest(seller.requests.element());

        //Assert
        Assertions.assertEquals(seller.products.size(), expectedAmountOfProductsSeller);
        Assertions.assertEquals(expectedRequestsSizeSeller, seller.requests.size());
        Assertions.assertEquals(expectedRequestsSizeCustomer, customer.requests.size());
        Assertions.assertFalse(request.isPaid());
        Assertions.assertSame(seller.getPartyType(), request.getPartyType());
        Assertions.assertEquals(expectedDoubleSpending, productChannel.getDoubleSpending().size());
    }

    @Test
    void processRequest_PROCESS_TEST_combination8() {
        //Arrange
        Party seller = new Seller("Bob", 0, 20);
        Customer customer = new Customer("Svejk", 400);

        ProductChannel productChannel = new ProductChannel(TransactionType.PRODUCT);
        Product beef1 = new Product(new MeatProduct(2, MeatProducts.BEEF));

        Request request = new Request(customer, new MeatProduct(2, MeatProducts.BEEF), PartyType.SELLER);

        int expectedAmountOfProductsSeller = 1;
        int expectedRequestsSizeSeller = 1;
        int expectedRequestsSizeCustomer = 0;
        int expectedDoubleSpending = 1;

        //Act
        seller.receiveProduct(beef1);

        productChannel.attach(seller);
        productChannel.attach(customer);

        productChannel.addRequest(request);

        productChannel.addSellTransaction(seller,customer, beef1, new MeatProduct(2, MeatProducts.BEEF));
        productChannel.addSellTransaction(seller,customer, beef1, new MeatProduct(2, MeatProducts.BEEF));

        //Main Act
        seller.fulfillRequest(seller.requests.element());

        //Assert
        Assertions.assertEquals(expectedAmountOfProductsSeller, seller.products.size());
        Assertions.assertTrue(productChannel.getSubscribers().contains(seller));
        Assertions.assertTrue(productChannel.getSubscribers().contains(customer));
        Assertions.assertEquals(expectedRequestsSizeSeller, seller.requests.size());
        Assertions.assertEquals(expectedRequestsSizeCustomer, customer.requests.size());
        Assertions.assertEquals(expectedDoubleSpending, productChannel.getDoubleSpending().size());

        Assertions.assertTrue(request.isPaid());
        Assertions.assertSame(customer, request.getCreator());
        Assertions.assertSame(seller.getPartyType(), request.getPartyType());
    }

    @Test
    void processRequest_PROCESS_TEST_combination4() {
        //Arrange
        Party seller = new Seller("Bob", 0, 20);
        Party customer = new Customer("Svejk", 400);

        ProductChannel productChannel = new ProductChannel(TransactionType.PRODUCT);

        Request request = new Request(customer, new MeatProduct(2, MeatProducts.BEEF), PartyType.SELLER);

        int expectedAmountOfProductsSeller = 0;
        int expectedRequestsSizeSeller = 1;
        int expectedRequestsSizeCustomer = 0;
        int expectedDoubleSpending = 0;


        //Act
        productChannel.attach(seller);
        productChannel.attach(customer);

        productChannel.addRequest(request);


        //Main Act
        seller.fulfillRequest(request);

        //Assert
        Assertions.assertEquals(seller.products.size(), expectedAmountOfProductsSeller);
        Assertions.assertTrue(productChannel.getSubscribers().contains(seller));
        Assertions.assertTrue(productChannel.getSubscribers().contains(customer));
        Assertions.assertEquals(expectedRequestsSizeSeller, seller.requests.size());
        Assertions.assertEquals(expectedRequestsSizeCustomer, customer.requests.size());
        Assertions.assertEquals(expectedDoubleSpending, productChannel.getDoubleSpending().size());

        Assertions.assertFalse(request.isPaid());
        Assertions.assertSame(request.getCreator(), customer);
        Assertions.assertSame(seller.getPartyType(), request.getPartyType());
        Assertions.assertTrue(seller.products.isEmpty());
    }
}