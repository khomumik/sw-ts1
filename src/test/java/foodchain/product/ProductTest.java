package foodchain.product;

import foodchain.product.Products.MeatProduct;
import foodchain.product.Products.MeatProducts;
import foodchain.product.Products.ProductType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ProductTest {

    @Test
    void split_OneProduct5Kg_TwoProducts2And3Kg() {
        //Arrange
        ProductType beef = new MeatProduct(5, MeatProducts.BEEF);
        Product product = new Product(beef);
        int expectedAmountNewProduct = 2;
        int expectedAmountProduct = 3;
        MeatProducts expectedProduct = MeatProducts.BEEF;
        //Act
        Product newProduct = product.split(2);
        //Assert
        assertEquals(expectedAmountProduct, product.getProductType().getQuantity());
        assertEquals(expectedAmountNewProduct, newProduct.getProductType().getQuantity());
        assertEquals(expectedProduct, newProduct.getProductType().getProductTypes());
        assertNotEquals(newProduct, product);   // 67 line in Prodduct (When i was writing that function I had a problem
        // that I was returning the same object but with reduced quantity)
    }
}