package foodchain.party.channels;

import foodchain.party.ChannelObserver;
import foodchain.party.Party;
import foodchain.transactions.GenesisTransaction;
import foodchain.transactions.Transaction;
import foodchain.transactions.TransactionType;

import java.util.*;

/**
 * The type Channel.
 */
public abstract class Channel implements PartyObservable {
    protected TransactionType transactionType;
    protected GenesisTransaction genesisTransaction = new GenesisTransaction(null, null);
    protected Transaction lastTransaction = genesisTransaction;
    protected List<ChannelObserver> subscribers;
    protected Map<Party, Integer> doubleSpending = new HashMap<>();

    /**
     * Instantiates a new Channel.
     *
     * @param type the transaction type
     */
    public Channel(TransactionType type) {
        this.subscribers = new LinkedList<>();
        this.transactionType = type;
    }

    public Map<Party, Integer> getDoubleSpending() {
        return new HashMap<>(doubleSpending);
    }

    public List<ChannelObserver> getSubscribers() {
        return new ArrayList<>(subscribers);
    }

    /**
     * Gets transaction type of this transaction.
     *
     * @return the transaction type
     */
    public TransactionType getTransactionType() {
        return transactionType;
    }

    @Override
    public void attach(ChannelObserver channelObserver) {
        if (!subscribers.contains(channelObserver)) {
            subscribers.add(channelObserver);
            channelObserver.attach(this);
        }

    }

    @Override
    public void detach(ChannelObserver channelObserver) {
        if (subscribers.contains(channelObserver)) {
            subscribers.remove(channelObserver);
            channelObserver.detach(this);
        }

    }

}
